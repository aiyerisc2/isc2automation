<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_LF05-06-2020-03-38-37 Lead Form</name>
   <tag></tag>
   <elementGuidId>9976ee90-1fa3-48b1-bbe3-7b1d5250f7e3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='brandBand_1']/div/div/div[2]/div/div[2]/div/div/div/div/div/div/div/div[2]/div/div/div/div[2]/div[2]/div/div/div/table/tbody/tr/th/span/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>slds-truncate outputLookupLink slds-truncate outputLookupLink-00Q2g000001nkskEAA-354:1835;a forceOutputLookup</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ownerid</name>
      <type>Main</type>
      <value>354:1835;a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-refid</name>
      <type>Main</type>
      <value>recordId</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-recordid</name>
      <type>Main</type>
      <value>00Q2g000001nkskEAA</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-special-link</name>
      <type>Main</type>
      <value>true</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-navigable</name>
      <type>Main</type>
      <value>true</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/lightning/r/00Q2g000001nkskEAA/view</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>target</name>
      <type>Main</type>
      <value>_blank</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>rel</name>
      <type>Main</type>
      <value>noreferrer</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>LF05-06-2020-03-38-37 Lead Form</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-aura-rendered-by</name>
      <type>Main</type>
      <value>365:1835;a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-aura-class</name>
      <type>Main</type>
      <value>forceOutputLookup</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>LF05-06-2020-03-38-37 Lead Form</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;brandBand_1&quot;)/div[@class=&quot;slds-template__container&quot;]/div[@class=&quot;center oneCenterStage lafSinglePaneWindowManager&quot;]/div[@class=&quot;windowViewMode-normal oneContent active lafPageHost&quot;]/div[@class=&quot;slds-grid forceSearchBaseSearchPage forceSearchSearchPageDesktop&quot;]/div[@class=&quot;topResultsWrapper&quot;]/div[@class=&quot;resultsWrapper slds-template__content slds-scrollable--y&quot;]/div[@class=&quot;forceSearchResultsRegion slds-grid slds-grid--vertical&quot;]/div[@class=&quot;enabled uiScroller forceSearchResultsMultiScope scroller-wrapper scroll-vertical native&quot;]/div[@class=&quot;scroller&quot;]/div[@class=&quot;resultsMultiWrapper&quot;]/div[@class=&quot;slds-grid slds-grid_vertical&quot;]/div[@class=&quot;resultsItem slds-col slds-no-flex slds-m-bottom_small&quot;]/div[@class=&quot;slds-table_joined   test-listViewManager slds-card slds-card_boundary slds-grid slds-grid--vertical forceListViewManager forceSearchResultsGridLVM&quot;]/div[@class=&quot;slds-grid listDisplays safari-workaround-anchor&quot;]/div[@class=&quot;listViewContainer safari-workaround&quot;]/div[@class=&quot;slds-col slds-no-space forceListViewManagerPrimaryDisplayManager&quot;]/div[@class=&quot;autoHeight col-0-wrap col-1-wrap col-2-wrap col-3-wrap col-4-wrap col-5-wrap col-6-wrap col-7-wrap hideRowNumberColumn hideSelection forceListViewManagerGrid&quot;]/div[@class=&quot;listViewContent slds-table--header-fixed_container&quot;]/div[@class=&quot;uiScroller scroller-wrapper scroll-bidirectional native&quot;]/div[@class=&quot;scroller actionBarPlugin&quot;]/div[@class=&quot;slds-table--edit_container slds-is-relative  inlineEdit--disabled keyboardMode--active inlineEditGrid forceInlineEditGrid&quot;]/table[@class=&quot;slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable&quot;]/tbody[1]/tr[1]/th[@class=&quot;slds-cell-edit cellContainer&quot;]/span[@class=&quot;slds-grid slds-grid--align-spread&quot;]/a[@class=&quot;slds-truncate outputLookupLink slds-truncate outputLookupLink-00Q2g000001nkskEAA-354:1835;a forceOutputLookup&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='brandBand_1']/div/div/div[2]/div/div[2]/div/div/div/div/div/div/div/div[2]/div/div/div/div[2]/div[2]/div/div/div/table/tbody/tr/th/span/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'LF05-06-2020-03-38-37 Lead Form')])[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Action'])[2]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Column Menu'])[16]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='AutomatedLeadForm'])[2]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Automation Test'])[2]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/lightning/r/00Q2g000001nkskEAA/view')])[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div[2]/div[2]/div/div/div/table/tbody/tr/th/span/a</value>
   </webElementXpaths>
</WebElementEntity>
