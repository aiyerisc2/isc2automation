<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>selectCountry</name>
   <tag></tag>
   <elementGuidId>7a52aeb9-c253-4a2c-850c-33fdbe107c00</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[8]/li/div/div/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control country</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>country</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>country</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-text</name>
      <type>Main</type>
      <value>Country</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bind</name>
      <type>Main</type>
      <value>value: selectedCountry</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Country/Region</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                            Country/Region
                                                                                Afghanistan
                                                                                Aland Islands
                                                                                Albania
                                                                                Algeria
                                                                                Andorra
                                                                                Angola
                                                                                Anguilla
                                                                                Antarctica
                                                                                Antigua and Barbuda
                                                                                Argentina
                                                                                Armenia
                                                                                Aruba
                                                                                Australia
                                                                                Austria
                                                                                Azerbaijan
                                                                                Bahamas
                                                                                Bahrain
                                                                                Bangladesh
                                                                                Barbados
                                                                                Belarus
                                                                                Belgium
                                                                                Belize
                                                                                Benin
                                                                                Bermuda
                                                                                Bhutan
                                                                                Bolivia, Plurinational State of
                                                                                Bonaire, Sint Eustatius and Saba
                                                                                Bosnia and Herzegovina
                                                                                Botswana
                                                                                Bouvet Island
                                                                                Brazil
                                                                                British Indian Ocean Territory
                                                                                Brunei Darussalam
                                                                                Bulgaria
                                                                                Burkina Faso
                                                                                Burundi
                                                                                Cambodia
                                                                                Cameroon
                                                                                Canada
                                                                                Cape Verde
                                                                                Cayman Islands
                                                                                Central African Republic
                                                                                Chad
                                                                                Chile
                                                                                China
                                                                                Chinese Taipei
                                                                                Christmas Island
                                                                                Cocos (Keeling) Islands
                                                                                Colombia
                                                                                Comoros
                                                                                Congo
                                                                                Congo, the Democratic Republic of the
                                                                                Cook Islands
                                                                                Costa Rica
                                                                                Cote d'Ivoire
                                                                                Croatia
                                                                                Curaçao
                                                                                Cyprus
                                                                                Czech Republic
                                                                                Denmark
                                                                                Djibouti
                                                                                Dominica
                                                                                Dominican Republic
                                                                                Ecuador
                                                                                Egypt
                                                                                El Salvador
                                                                                Equatorial Guinea
                                                                                Eritrea
                                                                                Estonia
                                                                                Ethiopia
                                                                                Falkland Islands (Malvinas)
                                                                                Faroe Islands
                                                                                Fiji
                                                                                Finland
                                                                                France
                                                                                French Guiana
                                                                                French Polynesia
                                                                                French Southern Territories
                                                                                Gabon
                                                                                Gambia
                                                                                Georgia
                                                                                Germany
                                                                                Ghana
                                                                                Gibraltar
                                                                                Greece
                                                                                Greenland
                                                                                Grenada
                                                                                Guadeloupe
                                                                                Guatemala
                                                                                Guernsey
                                                                                Guinea
                                                                                Guinea-Bissau
                                                                                Guyana
                                                                                Haiti
                                                                                Heard Island and McDonald Islands
                                                                                Holy See (Vatican City State)
                                                                                Honduras
                                                                                Hong Kong
                                                                                Hungary
                                                                                Iceland
                                                                                India
                                                                                Indonesia
                                                                                Iraq
                                                                                Ireland
                                                                                Isle of Man
                                                                                Israel
                                                                                Italy
                                                                                Jamaica
                                                                                Japan
                                                                                Jersey
                                                                                Jordan
                                                                                Kazakhstan
                                                                                Kenya
                                                                                Kiribati
                                                                                Korea, Republic of
                                                                                Kuwait
                                                                                Kyrgyzstan
                                                                                Lao People's Democratic Republic
                                                                                Latvia
                                                                                Lebanon
                                                                                Lesotho
                                                                                Liberia
                                                                                Libyan Arab Jamahiriya
                                                                                Liechtenstein
                                                                                Lithuania
                                                                                Luxembourg
                                                                                Macao
                                                                                Macedonia, the former Yugoslav Republic of
                                                                                Madagascar
                                                                                Malawi
                                                                                Malaysia
                                                                                Maldives
                                                                                Mali
                                                                                Malta
                                                                                Martinique
                                                                                Mauritania
                                                                                Mauritius
                                                                                Mayotte
                                                                                Mexico
                                                                                Moldova, Republic of
                                                                                Monaco
                                                                                Mongolia
                                                                                Montenegro
                                                                                Montserrat
                                                                                Morocco
                                                                                Mozambique
                                                                                Myanmar
                                                                                Namibia
                                                                                Nauru
                                                                                Nepal
                                                                                Netherlands
                                                                                New Caledonia
                                                                                New Zealand
                                                                                Nicaragua
                                                                                Niger
                                                                                Nigeria
                                                                                Niue
                                                                                Norfolk Island
                                                                                Norway
                                                                                Oman
                                                                                Pakistan
                                                                                Palestinian Territory, Occupied
                                                                                Panama
                                                                                Papua New Guinea
                                                                                Paraguay
                                                                                Peru
                                                                                Philippines
                                                                                Pitcairn
                                                                                Poland
                                                                                Portugal
                                                                                Qatar
                                                                                Reunion
                                                                                Romania
                                                                                Russian Federation
                                                                                Rwanda
                                                                                Saint Barthélemy
                                                                                Saint Helena, Ascension and Tristan da Cunha
                                                                                Saint Kitts and Nevis
                                                                                Saint Lucia
                                                                                Saint Martin (French part)
                                                                                Saint Pierre and Miquelon
                                                                                Saint Vincent and the Grenadines
                                                                                Samoa
                                                                                San Marino
                                                                                Sao Tome and Principe
                                                                                Saudi Arabia
                                                                                Senegal
                                                                                Serbia
                                                                                Seychelles
                                                                                Sierra Leone
                                                                                Singapore
                                                                                Sint Maarten (Dutch part)
                                                                                Slovakia
                                                                                Slovenia
                                                                                Solomon Islands
                                                                                Somalia
                                                                                South Africa
                                                                                South Georgia and the South Sandwich Islands
                                                                                South Sudan
                                                                                Spain
                                                                                Sri Lanka
                                                                                Suriname
                                                                                Svalbard and Jan Mayen
                                                                                Swaziland
                                                                                Sweden
                                                                                Switzerland
                                                                                Tajikistan
                                                                                Tanzania, United Republic of
                                                                                Thailand
                                                                                Timor-Leste
                                                                                Togo
                                                                                Tokelau
                                                                                Tonga
                                                                                Trinidad and Tobago
                                                                                Tunisia
                                                                                Turkey
                                                                                Turkmenistan
                                                                                Turks and Caicos Islands
                                                                                Tuvalu
                                                                                Uganda
                                                                                Ukraine
                                                                                United Arab Emirates
                                                                                United Kingdom
                                                                                United States
                                                                                Uruguay
                                                                                Uzbekistan
                                                                                Vanuatu
                                                                                Venezuela, Bolivarian Republic of
                                                                                Viet Nam
                                                                                Virgin Islands, British
                                                                                Wallis and Futuna
                                                                                Western Sahara
                                                                                Yemen
                                                                                Zambia
                                                                                Zimbabwe
                                                                        </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;country&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='country']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='training-search']/div/div/form/fieldset/ul/div/li/div/div/div/ul/div/div[8]/li/div/div/select</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Country/Region'])[1]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='City'])[1]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='State'])[1]/preceding::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Province'])[1]/preceding::select[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/li/div/div/select</value>
   </webElementXpaths>
</WebElementEntity>
