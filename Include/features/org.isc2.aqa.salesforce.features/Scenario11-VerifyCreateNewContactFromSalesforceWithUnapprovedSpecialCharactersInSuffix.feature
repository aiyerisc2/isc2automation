Feature: Scenario-11 Create New Contact from Salesforce with unapproved Special Characters in suffix is not successful

  Scenario Outline: Verify that creating a contact from Salesforce with unapproved Special Characters in suffix field is not successful
    Given I open a browser
    When I'm on salesforce
    And I enter the login credentials
    And I click the login button
    Then I see the login is successful
    And I navigate to contacts page
    And I create a new contact
    And I enter first name as <firstNameValue>
    And I enter middle name as <middleNameValue>
    And I enter last name as <lastNameValue>
    And I enter suffix as <suffixValue>
    And I enter email address
    And I click save
    Then I see error message for unapproved special characters
    	Examples:
    		|suffixValue			 |firstNameValue|lastNameValue|middleNameValue|
    		|I/SC2Suffix 	 		 |Test          |Automation		|MiddleNameValue|
    		|I\SC2Suffix			 |Test 				  |Automation		|MiddleNameValue|
    		|I,SC2Suffix       |Test				  |Automation		|MiddleNameValue|
    		|I<SC2Suffix			 |Test				  |Automation		|MiddleNameValue|
    		|I>SC2Suffix  		 |Test				  |Automation		|MiddleNameValue|
    		|I"SC2Suffix  		 |Test				  |Automation		|MiddleNameValue|
  