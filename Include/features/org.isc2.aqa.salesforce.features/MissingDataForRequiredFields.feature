Feature: Scenario- Creating a contact with missing first name is not successful

  Scenario: Verify that creating a contact with missing first name is not successful
    Given I open a browser
    When I'm on salesforce
    And I enter the login credentials
    And I click the login button
    Then I see the login is successful
    And I navigate to contacts page
    And I create a new contact
    And I click save
    Then I see an error message for missing first name
    And I see an error message for missing last name