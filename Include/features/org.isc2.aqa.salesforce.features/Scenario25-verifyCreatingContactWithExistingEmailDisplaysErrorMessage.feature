Feature: Scenario-25 Create New Contact from Salesforce using Email that exist in Salesforce

  Scenario Outline: Verify that creating a contact with existing email address displays error message and is unable to create a contact
  	Given I open a browser
    When I'm on salesforce
    And I enter the login credentials
    And I click the login button
    Then I see the login is successful
    And I navigate to contacts page
    And I create a new contact
    And I enter first name as <firstNameValue>
    And I enter last name as <lastNameValue>
    And I enter email address as <emailAddress>
    And I click save  
    Then I see a duplicate contact error message
    And I close a browser
    	Examples:
    		|firstNameValue|lastNameValue|emailAddress	 															|
    		|Test 			 	 |User				 |DoNotDeleteUsedForAutomation@mailinator.com |
