Feature: Member verification works as expected

  Scenario: Verify that member verification works as expected
    Given I open a browser 
    When I navigate to member verification page
    And I enter last name field in the member verification form
    And I enter member number in the member verification form
    And I search for the member in member verification form
    Then I see the correct member name in the search result
    And I see the correct certification in the search result
    And I see correct Active Date in the search results
    And I see correct Expiration Date in the search results