Feature: Scenario- Filling a Lead Form and verifying the lead form data in salesforce

  Scenario: Verify that filling a lead form is susccessful via SiteCore and lead details exist in SalesForce
    Given I open a browser
    When I navigate to private-on-site lead form
 		And I enter the first name in private-on-site lead form
 		And I enter the last name in private-on-site lead form
 		And I enter the job title in private-on-site lead form
 		And I enter the company in private-on-site lead form
 		And I select the type of industry in private-on-site lead form
 		And I enter the email in private-on-site lead form
 		And I enter the phone number in private-on-site lead form
 		And I select Country in private-on-site lead form
 		And I select State or Province in private-on-site lead form
  	And I select Ceritification of Interest in private-on-site lead form
  	And I select Number of Employees to be trained in private-on-site lead form
  	And I select Time for Training in private-on-site lead form 
  	And I have read and agree to the privacy policy in private-on-site lead form
  	And I submit private-on-site lead form
  	Then I see the private-on-site lead form is submitted successfully
  	And I end the session
  	When I'm on salesforce
    And I enter the login credentials
    And I click the login button
    Then I see the login is successful
    And I navigate to Leads Page
    And I search for the lead that was just submitted
    Then I see the correct job title for the submitted lead
    And I see the correct company name for the submitted lead
    And I see the correct phone number for the submitted lead
    And I see the correct email for the submitted lead
    And I see the lead owner as message bus service
    And I close a browser
    
    
    
  	
    
    
    
  	
