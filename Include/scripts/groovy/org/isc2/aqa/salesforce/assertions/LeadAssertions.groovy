package org.isc2.aqa.salesforce.assertions
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

import org.isc2.aqa.salesforce.stepDefinitions.BaseSteps


class LeadAssertions extends BaseSteps {

	@Then("I see the correct job title for the submitted lead")
	def iSeeCorrectTitleForSubmittedLead() {
		WebUI.delay(20)
		WebUI.verifyTextPresent('AutomatedLeadForm', false)
		//WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/Page_LF05-06-2020-03-38-37 Lead Form  Salesforce/lightning-formatted-text_AutomatedLeadForm'))
		//WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/Page_LF05-06-2020-03-38-37 Lead Form  Salesforce/lightning-formatted-text_Automation Test'))
		//WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/Page_LF05-06-2020-03-38-37 Lead Form  Salesforce/a_(123) 456-7899'))

		//WebUI.verifyElementText('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/Page_LF05-06-2020-03-38-37 Lead Form  Salesforce/lightning-formatted-text_AutomatedLeadForm', 'AutomatedLeadForm')
	}

	@Then("I see the correct company name for the submitted lead")
	def iSeeCorrectCompanyNameForSubmittedLead() {
		WebUI.verifyTextPresent('Automation Test', false)
	}

	@Then("I see the correct phone number for the submitted lead")
	def iSeeCorrectPhoneNumberForSubmittedLead() {
		//WebUI.delay(20)
		WebUI.verifyTextPresent('1234567899', false)
	}

	@Then("I see the correct certification of interest for the submitted lead")
	def iSeeCorrectCOI() {
		WebUI.verifyTextPresent('CSSLP', false)
	}

	@Then("I see the correct email for the submitted lead")
	def iSeeCorrectEmailForSubmittedLead() {
		WebUI.verifyTextPresent(GlobalVariable.leadFormemail, false)
		//WebUI.delay(20)
		//WebUI.verifyTextPresent('lf05-13-2020-11-27-13@mailinator.com', false)
	}

	@Then("I see the correct number of employees to be trained for the submitted lead")
	def iSeeCorrectNoOfEmployeesForSubmittedLead() {
		WebUI.delay(20)
		WebUI.verifyTextPresent('10-19', false)
	}

	@Then("I see the correct timeline for training for the submitted lead")
	def iSeeCorrectTimelineForTrainingForSubmittedLead() {
		WebUI.verifyTextPresent('4-6 Months', false)
	}

	@Then("I see the lead owner as message bus service")
	def iSeeLeadOwnerMessageBusService() {
		//tring ExpectedName = WebUI.getText(findTestObject('Object Repository/LEAD ASSERTIONS/Page_LF05-13-2020-12-17-26 Lead Form  Salesforce/div_Lead OwnerMessageBus ServiceOpen Messag_4f0218'))
		//WebUI.click(findTestObject('Object Repository/LEAD ASSERTIONS/Page_LF05-13-2020-12-17-26 Lead Form  Salesforce/div_Lead OwnerMessageBus ServiceOpen Messag_4f0218'))
		//println('WOHOOOOOOOOOO' + ExpectedName)
		WebUI.verifyTextPresent('mserv', false)
	}
	
	@Then("I see CREATED for Okta status")
	def iSeeWebDBtatus() {
		
		String ExpectedName = WebUI.getText(findTestObject('Object Repository/org.isc2.aqa.salesforce.pageObjects/ContactsPage/OktaStatus'))
		println('Ashwin' + ExpectedName) 
		WebUI.verifyElementText(findTestObject('Object Repository/org.isc2.aqa.salesforce.pageObjects/ContactsPage/OktaStatus'),
			'CREATED')
	}
	
	@Then("I see CREATED for WebDB status")
	def iSeeWebDBStatus() {
		WebUI.verifyElementText(findTestObject('Object Repository/org.isc2.aqa.salesforce.pageObjects/ContactsPage/WebDBStatus'),
			'CREATED')
		}
	
	@Then("I see CREATED for Salesforce status")
	def iSeeSalesforceStatus() {
		WebUI.verifyElementText(findTestObject('Object Repository/org.isc2.aqa.salesforce.pageObjects/ContactsPage/SalesforceStatus'),
			'CREATED')
	}
	
	
	
}