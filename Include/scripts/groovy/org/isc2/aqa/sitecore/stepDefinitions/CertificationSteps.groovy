package org.isc2.aqa.sitecore.stepDefinitions
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

import org.isc2.aqa.salesforce.stepDefinitions.BaseSteps


class CertificationSteps extends BaseSteps {

	@And("I navigate to home page")
	def navigateToMemberVerificationPage() {
		WebUI.navigateToUrl(GlobalVariable.URL)
	}

	@And("I accept the cookies")
	def accpetCookies() {
		WebUI.waitForPageLoad(30)
		WebUI.click(findTestObject('Object Repository/SiteCore/HomePage/cookiesToastContainer'))
	}

	@And("I click the certifications drop-down")
	def clickCertificationsDropDown() {
		WebUI.waitForElementVisible(findTestObject('SiteCore/HomePage/a_Certifications'), 30)
		WebUI.mouseOver(findTestObject('SiteCore/HomePage/a_Certifications'))
		WebUI.click(findTestObject('SiteCore/HomePage/a_Certifications'))
	}

	@And("I click Register for an Exam")
	def clickRegisterForExam() {
		WebUI.waitForElementVisible(findTestObject('SiteCore/CertificationsPages/RegistrationPage/Page_Cybersecurity and IT Security Certific_16a664/a_Register for an Exam                     _59a42d'),10)
		WebUI.click(findTestObject('SiteCore/CertificationsPages/RegistrationPage/Page_Cybersecurity and IT Security Certific_16a664/a_Register for an Exam                     _59a42d'))
	}

	@And("I click Endorsement")
	def clickEndorsement() {
		WebUI.click(findTestObject('Verify that navigation to Certification page is successful/Page_Register for Cybersecurity Exam  Find _9de3f9/a_Endorsement                              _e0276b'))
	}

	@And("I click Member Verification")
	def clickMemberVerification() {
		WebUI.click(findTestObject('Verify that navigation to Certification page is successful/Page_Endorsement  Online Endorsement Applic_ed4e5e/a_Member Verification                      _02e1f7'))
	}

	@And("I click Digital Badges")
	def clickDigitalBadges() {
		WebUI.click(findTestObject('Verify that navigation to Certification page is successful/Page_(ISC) Digital Badges from Acclaim  (ISC)/a_Digital Badges                           _fa77f7'))
	}

	@And("I click CISSP")
	def clickCISSP() {
		WebUI.click(findTestObject('SiteCore/HomePage/Page_Cybersecurity and IT Security Certific_16a664/a_CISSP                                    _4e0bb0'))
	}

	@And("I click SSCP")
	def clickSSCP() {
		WebUI.click(findTestObject('SiteCore/HomePage/Page_IT Security Certification  SSCP - Syst_6d35f5/a_SSCP                                     _c7aa66'))
	}

	@And("I click CCSP")
	def clickCCSP() {
		WebUI.click(findTestObject('SiteCore/HomePage/Page_IT Security Certification  SSCP - Syst_6d35f5/a_CCSP                                     _cb9bc3'))
	}

	@And("I click CAP")
	def clickCAP() {
		WebUI.click(findTestObject('SiteCore/HomePage/Page_Cloud Security Certification  CCSP - C_4e65b7/a_CAP                                      _daeb5a'))
	}

	@And("I click CISSP Concentrations")
	def clickCISSPConcentrations() {
		WebUI.click(findTestObject('SiteCore/HomePage/Page_IT Security Architect Engineer and Man_20c9f5/a_CISSP Concentrations                     _d8be5c'))
	}

	@And("I click HCISSP")
	def clickHCISSP() {
		WebUI.click(findTestObject('SiteCore/HomePage/Page_Healthcare Security Certification  HCI_566946/a_HCISPP                                   _33b0e2'))
	}

	@And("I click Associate of ISC2")
	def clickAssociateOfISC2() {
		WebUI.click(findTestObject('SiteCore/HomePage/Page_IT Security Architect Engineer and Man_20c9f5/a_Associate of (ISC)                       _63941e'))
	}
}
