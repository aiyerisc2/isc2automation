package org.isc2.aqa.sitecore.stepDefinitions
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.openqa.selenium.support.ui.WebDriverWait as WebDriverWait
import org.isc2.aqa.salesforce.stepDefinitions.BaseSteps

import org.openqa.selenium.support.ui.ExpectedConditions as ExpectedConditions
import org.junit.Assert


class CreateAccountSteps extends BaseSteps {

	String createAccountEmail = new String(((('ca' + todaysDate) + '-') + currentTime) + '@mailinator.com')

	@When("I'm on sitecore")
	def iMOnSiteCore() {
		WebUI.navigateToUrl(GlobalVariable.URL)
		//WebUI.openBrowser(GlobalVariable.URL)
	}

	@And("I click sign-in button on home page")
	def clickSignInButtonOnHomePage() {
		WebUI.click(findTestObject('SiteCore/CreateAccountPage/SignInButtonOnHomePage'))
	}

	@And("I click the create an account button")
	def clickCreateNewAccountButton() {
		WebDriver driver = DriverFactory.getWebDriver()
		WebDriverWait waitForVisibilityOfElement = new WebDriverWait(driver, LONG_TIMEOUT)
		waitForVisibilityOfElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath('//div[2]/div/a')))
		WebUI.click(findTestObject('SiteCore/CreateAccountPage/a_Create An Account'))
	}

	@And("I enter first name for creating an account")
	def enterFirstNameForCreatingAnAccount() {
		WebUI.setText(findTestObject('SiteCore/CreateAccountPage/firstNameField'), sitecoreActualName)
		println("First Name:-" + sitecoreActualName)
		GlobalVariable.actualName = sitecoreActualName
		println(GlobalVariable.actualName)
	}

	@And("I enter first name using (.*)")
	def enterFirstNameUsing(String firstNameValue) {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/SiteCore/CreateAccountPage/firstNameParametrized'), firstNameValue)
		println("First Name:-" + firstNameValue)
	}

	@And("I enter last name for creating an account")
	def enterLastNameForCreatingAnAccount() {
		WebUI.setText(findTestObject('SiteCore/CreateAccountPage/lastNameField'), 'AutomatedCreatedAccount')

		//println("Last Name:-AutomatedCreatedAccount")
		//String string = "AutomatedCreatedAccount";
		//....int count = 0;

		//Counts each character except space
		//for(int i = 0; i < string.length(); i++) {
		//if(string.charAt(i) != ' ')
		//		count++;
		//}
		//println("Total number of characters in a String" + count)
		//int expectedValue = 23
		//Assert.assertArrayEquals(expectedValue, count)
		//Assert.
	}

	@And("I enter last name using (.*)")
	def enterLastameUsing(String lastNameValue) {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/SiteCore/CreateAccountPage/lastNameParametrized'), lastNameValue)
		println("Last Name:-" + lastNameValue)
	}

	@And("I enter boundary (.*)")
	def enterBoundaryValues(String lastName) {
		println("Last Name:-" + lastName)
	}

	@And("I get the text of first name")
	def getTextForFirstName() {
		//String result = WebUI.getText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/SiteCore/CreateAccountPage/firstNameParametrized'))
		String lastName = WebUI.getText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/SiteCore/CreateAccountPage/lastNameParametrized'))
		println("Last name text after entering is:-" + lastName)
		GlobalVariable.lastName = lastName
		println(GlobalVariable.lastName)
	}

	@And("I enter an email for creating an account")
	def enterEmailForCreatingAnAccount() {
		WebUI.setText(findTestObject('SiteCore/CreateAccountPage/emailField'), createAccountEmail)
		GlobalVariable.createAccountEmail = createAccountEmail
		//println('Email used while creating an account:-', + createAccountEmail)
		//WebUI.setText(findTestObject('SiteCore/CreateAccountPage/emailField'), 'lf05-15-2020-06-07-47@mailinator.com')
	}

	@And("I enter an email for creating an account using upper boundary (.*)")
	def enterEmailForCreatingAnAccountExceeding(String emailValue) {
		String emailExceeding80Characters = new String((((emailValue + todaysDate) + '-') + currentTime) + '@mailinator.com')
		WebUI.setText(findTestObject('SiteCore/CreateAccountPage/emailField'), emailExceeding80Characters)
		println('Email exceeding 80 characters :-' + emailExceeding80Characters)
	}

	@And("I confirm email for creating an account")
	def confirmEmailForCreatingAnAccount() {
		WebUI.setText(findTestObject('SiteCore/CreateAccountPage/confirmEmailField'), createAccountEmail)
		//WebUI.setText(findTestObject('SiteCore/CreateAccountPage/confirmEmailField'), 'lf05-15-2020-06-07-47@mailinator.com')
	}

	@And("I confirm email for creating an account using upper boundary (.*)")
	def confirmEmailForCreatingAnAccountExceeding(String confirmEmailValue) {
		//WebUI.setText(findTestObject('SiteCore/CreateAccountPage/confirmEmailField'), confirmEmailValue)
		WebUI.setText(findTestObject('SiteCore/CreateAccountPage/confirmEmailField'), 'lf05-15-2020-06-11-31@mailinator.com')
	}

	@And("I confirm email address as (.*)")
	def confirmEmailAddressAs(String confirmEmailAddress) {
		WebUI.setText(findTestObject('SiteCore/CreateAccountPage/confirmEmailField'), confirmEmailAddress)
	}

	@And("I enter passwrd for creating an account")
	def enterPasswordForCreatingAnAccount() {
		//WebUI.setText(findTestObject('SiteCore/CreateAccountPage/paswordField'), ((('Ca' + todaysDate) + '-') + currentTime) + '@mailinator.com')
		//WebUI.setText(findTestObject('SiteCore/CreateAccountPage/paswordField'), ((('P@ssw0rd!' + todaysDate) + '-') + currentTime))
		WebUI.setText(findTestObject('SiteCore/CreateAccountPage/paswordField'),'P@ssw0rd!')
	}
	
	@And("I enter password that does not meet password requirements")
	def enterPasswordDoesNotMeetPasswordRequirements() {
		//WebUI.setText(findTestObject('SiteCore/CreateAccountPage/paswordField'), ((('Ca' + todaysDate) + '-') + currentTime) + '@mailinator.com')
		//WebUI.setText(findTestObject('SiteCore/CreateAccountPage/paswordField'), ((('P@ssw0rd!' + todaysDate) + '-') + currentTime))
		WebUI.setText(findTestObject('SiteCore/CreateAccountPage/paswordField'),'P!')
	}

	@And("I confirm password for creating an account")
	def confirmPasswordForCreatingAnAccount() {
		//WebUI.setText(findTestObject('SiteCore/CreateAccountPage/confirmPasswordField'), ((('P@ssw0rd!' + todaysDate) + '-') + currentTime))
		WebUI.setText(findTestObject('SiteCore/CreateAccountPage/confirmPasswordField'),'P@ssw0rd!')
	}

	@And("I click create a new account button")
	def clickCreatingANewAccountButton() {
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-CreateAccount/Scenario-CreateAccount/SuccessfulMessage-') +
				todaysDate) + '_') + currentTime) + '.PNG')
		WebUI.click(findTestObject('SiteCore/CreateAccountPage/createNewAccountButton'))
	}

	@And("I agree to the privacy policy")
	def agreePrivacyPolicy() {
		WebUI.click(findTestObject('SiteCore/CreateAccountPage/privacyPolicyCheckbox'))
	}

	@And("I enter first name with thirty nine characters")
	def enterFirstNameUsing39Characters() {
		WebUI.setText(findTestObject('SiteCore/CreateAccountPage/firstNameField'), 'firstNameUsing39CharactersfirstNameUsin')
	}

	@And("I enter first name with (.*) characters")
	def enterFirstNameWith40Characters(String charlen) {
		charlen = 'firstNameUsing40CharactersfirstNameUsing'
		WebUI.setText(findTestObject('SiteCore/CreateAccountPage/firstNameField'), charlen)
		println('Characters:-' + charlen)
	}

	@And("I enter first name with (.*) char")
	def enterFirstNameWith41Characters(String chara) {
		chara = 'firstNameUsing40CharactersfirstNameUsing1'
		WebUI.setText(findTestObject('SiteCore/CreateAccountPage/firstNameField'), chara)
		println('Characters:-' + chara)
	}

	@And("I leave all the required fields blank")
	def leaveAllTheRequiredFieldsBlank() {
	}

}