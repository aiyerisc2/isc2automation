import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.remote.server.DriverFactory as DriverFactory
import org.openqa.selenium.support.ui.ExpectedConditions as ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait as WebDriverWait
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.junit.After as After
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

Date today = new Date()

String todaysDate = today.format('MM-dd-yyyy')

String currentTime = today.format('hh-mm-ss')

String actualName = new String((('LF' + todaysDate) + '-') + currentTime)

int SHORT_TIMEOUT = 10

int TIMEOUT = 20

int LONG_TIMEOUT = 30

String currentDirectory = System.getProperty('user.dir')

WebUI.openBrowser('')

WebDriver driver = DriverFactory.getWebDriver()

WebDriverWait waitForVisibilityOfElement = new WebDriverWait(driver, LONG_TIMEOUT)

WebUI.navigateToUrl('https://wwwqa.isc2.org/Training/Private-On-Site')

//waitForVisibilityOfElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='close button']")))
WebUI.click(findTestObject('SiteCore/HomePage/cookiesToastContainer'))

waitForVisibilityOfElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath('//input[@id=\'wffma6c293203e1b4fc5be8b826edf4d0ab2_Sections_0__Fields_0__Value\']')))

WebUI.setText(findTestObject('SiteCore/LeadPages/firstNameField'), actualName)

WebUI.setText(findTestObject('SiteCore/LeadPages/lastNameField'), 'Lead Form')

WebUI.setText(findTestObject('SiteCore/LeadPages/jobTitleField'), 'AutomatedLeadForm')

WebUI.setText(findTestObject('SiteCore/LeadPages/companyField'), 'Automation Test')

WebUI.takeScreenshot(((((currentDirectory + 'KatalonTestScreenshots/TestCase-LeadForm/Scenario-VerifyLeadFormCreation/FormFilledContent_1') + 
    todaysDate) + '-') + currentTime) + '.PNG')

WebUI.selectOptionByValue(findTestObject('SiteCore/LeadPages/dropdownTypeOfIndustry'), 'Agriculture', true)

WebUI.scrollToElement(findTestObject('SiteCore/LeadPages/scrollEmailField'), 3)

WebUI.setText(findTestObject('SiteCore/LeadPages/scrollEmailField'), ((('LF' + todaysDate) + '-') + currentTime) + '@mailinator.com')

WebUI.scrollToElement(findTestObject('SiteCore/LeadPages/addressCountryField'), 3)

WebUI.setText(findTestObject('SiteCore/LeadPages/phoneNumberField'), '1234567899')

WebUI.selectOptionByValue(findTestObject('SiteCore/LeadPages/addressCountryField'), 'Australia', true)

WebUI.takeScreenshot(((((currentDirectory + 'KatalonTestScreenshots/TestCase-LeadForm/Scenario-VerifyLeadFormCreation/FormFilledContent_2') + 
    todaysDate) + '-') + currentTime) + '.PNG')

result = WebUI.getText(findTestObject('SiteCore/LeadPages/scrollEmailField'))

WebUI.setText(findTestObject('SiteCore/LeadPages/addressStateField'), 'MH')

WebUI.selectOptionByValue(findTestObject('SiteCore/LeadPages/dropdownCertificationOfInterest'), 'CSSLP', true)

WebUI.selectOptionByValue(findTestObject('SiteCore/LeadPages/dropdownNoOfEmployees'), '10-19', true)

WebUI.selectOptionByValue(findTestObject('SiteCore/LeadPages/dropdownTrainingTimeline'), '4-6 Months', true)

WebUI.click(findTestObject('SiteCore/LeadPages/agreePolicyCheckbox'))

WebUI.click(findTestObject('SiteCore/LeadPages/submitButton'))

waitForVisibilityOfElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath('//form[@id=\'wffma6c293203e1b4fc5be8b826edf4d0ab2\']//form[@id=\'wffma6c293203e1b4fc5be8b826edf4d0ab2\']')))

WebUI.verifyTextPresent('Thank you for filling in the form.', false)

WebUI.takeScreenshot(((((currentDirectory + 'KatalonTestScreenshots/TestCase-LeadForm/Scenario-VerifyLeadFormCreation/FormFilledContent_2') + 
    todaysDate) + '-') + currentTime) + '.PNG')

WebUI.closeBrowser()

